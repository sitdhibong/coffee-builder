<?php

namespace Coffee;

use Coffee\CoffeeBuilder;
use Coffee\Coffee;

class LatteCoffeeBuilder implements CoffeeBuilder {
  protected $milk = 2;
  protected $espresso = 1;

  public static function make(): self {
    return new LatteCoffeeBuilder();
  }

  public function build(): Coffee {
    $coffee = new Coffee();
    $coffee->milk = $this->milk;
    $coffee->espresso = $this->espresso;

    return $coffee;
  }

  public function milk(?int $milk): self {
    $this->milk = max(0, $milk);
    return $this;
  }

  public function espresso(?int $espresso): self {
    $this->espresso = max(0, $espresso);
    return $this;
  }

  public function hotWater(?int $hotWater): self {
    return $this;
  }

  public function sugar(?int $sugar): self {
    return $this;
  }
}

