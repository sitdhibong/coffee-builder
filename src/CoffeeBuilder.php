<?php

namespace Coffee;

use Coffee\Coffee;

interface CoffeeBuilder {
  public static function make(): self;
  public function build(): Coffee;
  public function milk(?int $milk): self;
  public function espresso(?int $espresso): self;
  public function hotWater(?int $hotWater): self;
  public function sugar(?int $sugar): self;
}
