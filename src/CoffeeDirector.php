<?php

namespace Coffee;

use Coffee\CoffeeBuilder;
use Coffee\Coffee;

class CoffeeDirector {

  protected $builder;

  public function builder(CoffeeBuilder $builder): self {
    $this->builder = $builder;

    return $this;
  }

  public function build(): Coffee {
    return $this->builder->build();
  }
}

