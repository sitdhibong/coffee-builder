<?php

use PHPUnit\Framework\TestCase;
use \Coffee\LatteCoffeeBuilder;
use \Coffee\Coffee;

class LatteCoffeeBuilderTest extends TestCase {
  public $builder;

  public function setUp(): void {
    $this->builder = new LatteCoffeeBuilder();
  }

  public function testInstanceCreation() {
    $coffee = $this->builder
                   ->build();

    $this->assertInstanceOf(
      Coffee::class,
      $coffee
    );

  }

  public function testIngredients() {
    $coffee = $this->builder
                   ->build();

    $this->assertEquals(1, $coffee->espresso);
    $this->assertEquals(2, $coffee->milk);
  }

  public function testCustomIngredients() {
    $coffee = $this->builder
                   ->milk(5)
                   ->build();

    $this->assertEquals(1, $coffee->espresso);
    $this->assertEquals(5, $coffee->milk);
  }

  public function testCustomMaliciousIngredients() {
    $coffee = $this->builder
                   ->milk(-1)
                   ->build();

    $this->assertEquals(1, $coffee->espresso);
    $this->assertEquals(0, $coffee->milk);
  }
}
