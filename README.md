# Design Patterns: Builder Pattern

## Composer

Download composer
```
$ wget -O composer https://getcomposer.org/download/latest-stable/composer.phar
$ chmod +x composer
$ ./composer --version
```

## Install all libs requires

```
$ composer install
```

## Prepare for PHPUnit

Download PHPUnit with command
```
$ wget -O phpunit https://phar.phpunit.de/phpunit-9.phar
$ chmod +x phpunit
$ ./phpunit --version
```

### Test with PHPUnit

All test cases are placed in `tests` folder with pattern `<ClasssName>Test.php` and executed by
```
$ ./phpunit
```

